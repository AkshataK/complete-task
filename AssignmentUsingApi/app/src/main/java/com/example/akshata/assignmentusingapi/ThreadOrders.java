package com.example.akshata.assignmentusingapi;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by akshata on 17/5/16.
 */
public class ThreadOrders extends AsyncTask<Object,Object,ArrayList<Orders>>{

    ProgressDialog progressDialog;

    Context context;
    Handler handler;

    public ThreadOrders(Context context, Handler handler) {
        this.context = context;
        this.handler = handler;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progressDialog = new ProgressDialog(context);

        progressDialog.setMessage("Downloading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    @Override
    protected ArrayList<Orders> doInBackground(Object... params) {
        Webutil webutil= new Webutil();

        return webutil.getOrders();
    }

    @Override
    protected void onPostExecute(ArrayList<Orders> orderses) {
        super.onPostExecute(orderses);

        progressDialog.dismiss();
        Message message = new Message();

        Log.d("arrOrders",orderses.toString());

        message.obj = orderses;
        handler.handleMessage(message);
    }



}
