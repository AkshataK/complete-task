package com.example.akshata.assignmentusingapi;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by akshata on 17/5/16.
 */
public class Orders implements Serializable{

    String order_id,order_status,shippingType,paymentType;
    ArrayList<Items> arrItems;
    ArrayList<User> arrUser;
    User user;
    int total_items;
    double total_price;

    public Orders(String order_id, String order_status, String shippingType, String paymentType, ArrayList<Items> arrItems, User user) {
        this.order_id = order_id;
        this.order_status = order_status;
        this.shippingType = shippingType;
        this.paymentType = paymentType;
        this.arrItems = arrItems;
        this.user=user;

        this.total_items = getTotal_items_count();
        this.total_price=getTotalPrice();
        this.arrUser = arrUser;

       // Log.d("ArrUser",arrUser.toString());

    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getShippingType() {
        return shippingType;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public ArrayList<Items> getArrItems() {
        return arrItems;
    }

    public void setArrItems(ArrayList<Items> arrItems) {
        this.arrItems = arrItems;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getTotal_items_count()
    {
        ArrayList<Items> arr_tepmItem = getArrItems();

        int total = 0;

        for (int i =0 ;i<arr_tepmItem.size();i++)
        {
            Items items = arr_tepmItem.get(i);


            total= total+items.getItem_count();
        }return total;
    }

    public  double getTotalPrice()
    {
        ArrayList<Items> arr_tepmItem = getArrItems();

        double totalPrice = 0;

        for (int i =0 ;i<arr_tepmItem.size();i++)
        {
            Items items = arr_tepmItem.get(i);



            totalPrice= totalPrice+ (Double.parseDouble( items.getItem_price())* items.getItem_count());
        }return totalPrice;


    }
}
