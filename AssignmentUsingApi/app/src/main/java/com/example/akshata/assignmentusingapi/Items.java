package com.example.akshata.assignmentusingapi;

import java.io.Serializable;

/**
 * Created by akshata on 17/5/16.
 */
public class Items implements Serializable{

    String item_name,item_price,description;

    int item_count;

    public Items(String item_name, String item_price, String description, int item_count) {
        this.item_name = item_name;
        this.item_price = item_price;
        this.description = description;
        this.item_count = item_count;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_price() {
        return item_price;
    }

    public void setItem_price(String item_price) {
        this.item_price = item_price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getItem_count() {
        return item_count;
    }

    public void setItem_count(int item_count) {
        this.item_count = item_count;
    }
}
