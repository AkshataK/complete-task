package com.example.akshata.assignmentusingapi;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends Activity{

    ExpandableListView expListView;
    ExpandableListAdapter listAdapter;
    ArrayList<Orders> listDataHeader;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        expListView = (ExpandableListView) findViewById(R.id.expandablelist);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        new ThreadOrders(MainActivity.this,new DownloadHandler()).execute();

        swipeRefreshLayout.setColorSchemeColors(Color.GRAY, Color.GREEN, Color.BLUE,
                Color.RED, Color.CYAN);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                //swipeRefreshLayout.setEnabled(false);
                swipeRefreshLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        listAdapter.notifyDataSetChanged();

                        Toast.makeText(MainActivity.this, "Refreshed", Toast.LENGTH_SHORT).show();
                        swipeRefreshLayout.setRefreshing(false);

                    }
                },5000);

            }
        });

    }

    private class DownloadHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            listDataHeader = (ArrayList<Orders>) msg.obj;

            listAdapter = new ExpandableListAdapter(MainActivity.this,listDataHeader);
            listAdapter.notifyDataSetChanged();
            expListView.setAdapter(listAdapter);


        }


    }

}
