package com.example.akshata.assignmentusingapi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by akshata on 17/5/16.
 */
public class Webutil {

   /* public static ArrayList<Orders> getData()
    {


       // public Orders(String profileImage, String userName, String orderNo, String orderStatus, String shippingType, String paymentType, ArrayList<Items> arrItems, ArrayList<User> arrUser) {


        ArrayList<Orders> orders = new ArrayList<>();
        orders.add(new Orders("http://devapi.onegreendiary.com:1337/profile/userProfile_269.png","V.Ganesh","OR2212","ORDS0007","ST0001","PAYT0002",getitem()));
        orders.add(new Orders("http://devapi.onegreendiary.com:1337/profile/userProfile_269.png","Yogesh.w","OR2213","ORDS0001","ST0002","PAYT0001",getitem()));
        orders.add(new Orders("http://devapi.onegreendiary.com:1337/profile/userProfile_269.png","V.Ganesh","OR2214","ORDS0002","ST0001","PAYT0002",getitem()));
        orders.add(new Orders("http://devapi.onegreendiary.com:1337/profile/userProfile_269.png","Rajkiran.s","OR2215","ORDS0007","ST0002","PAYT0001",getitem()));
        orders.add(new Orders("http://devapi.onegreendiary.com:1337/profile/userProfile_269.png","V.Ganesh","OR2216","ORDS0003","ST0001","PAYT0002",getitem()));
        orders.add(new Orders("http://devapi.onegreendiary.com:1337/profile/userProfile_269.png","Yogesh.w","OR2217","ORDS0001","ST0002","PAYT0001",getitem()));



        return orders;
    }

    public static ArrayList<Items> getitem()
    {
        ArrayList<Items> arrayitems = new ArrayList<>();

        arrayitems.add(new Items(2,20,"bread","10"));
        arrayitems.add(new Items(4,80,"cake","20"));

        return arrayitems;
    }*/


    public static ArrayList<Orders> getOrders()
    {
        ArrayList<Orders> arrayOrders = new ArrayList<>();
        try {
            //URL url = new URL("http://192.168.0.11:8000/order.json");

            URL url = new URL("http://devapi.onegreendiary.com:1337/merchant/orders_list");


             String api_key="12345" ,auth_token = "062274dedcc6a84e29c442cd4df6112a";

            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

             httpURLConnection.setRequestMethod("GET");

            httpURLConnection.setRequestProperty("api_key",api_key);
            httpURLConnection.setRequestProperty("auth_token",auth_token);


            BufferedReader reader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));

            StringBuffer buffer = new StringBuffer();

            String inline;

            while ((inline=reader.readLine())!=null)
            {
                buffer.append(inline);
            }

            //JSONArray jsonArray = new JSONArray(buffer.toString());

            JSONObject jsonObjects = new JSONObject(buffer.toString());

            JSONArray jsonArray = jsonObjects.getJSONArray("result");

            for (int i=0;i<jsonArray.length();i++)
            {

               // Orders orders = new Orders();

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                JSONObject orderObject = jsonObject.getJSONObject("order");

                String order_id,order_status,shippingType,paymentType;

               order_id= orderObject.getString("order_id");

               order_status = orderObject.getString("order_status");

                shippingType = orderObject.getString("shipping_type");

                paymentType = orderObject.getString("payment_type");


                ArrayList<Items> arrayItems = new ArrayList<>();


                JSONArray jsonArrayItems = jsonObject.getJSONArray("items");

                for (int j = 0;j<jsonArrayItems.length();j++)
                {
                    JSONObject itemObject = jsonArrayItems.getJSONObject(j);

                    String item_name,item_price,description;

                    int item_count;

                    item_name = itemObject.getString("item_name");
                    item_price = itemObject.getString("item_price");
                    description = itemObject.getString("description");
                    item_count = itemObject.getInt("item_count");



                    Items items = new Items(item_name,item_price,description,item_count);

                    arrayItems.add(items);
                }

               // ArrayList<User> arrayUser = new ArrayList<>();

                JSONArray jsonArrayUser = jsonObject.getJSONArray("user");




                    JSONObject userObject = jsonArrayUser.getJSONObject(0);

                    String name,profile_img_url;

                    name = userObject.getString("name");
                    profile_img_url = userObject.getString("profile_img_url");


                User user = new User(name,profile_img_url);

                    //arrayUser.add(user);





                Orders orders = new Orders(order_id,order_status,shippingType,paymentType,arrayItems,user);

                arrayOrders.add(orders);


            }return arrayOrders;



        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }return null;
    }

}
