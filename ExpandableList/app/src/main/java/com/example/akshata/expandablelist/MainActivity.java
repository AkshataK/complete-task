package com.example.akshata.expandablelist;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends Activity {

    ExpandableListView expListView;
    ExpandableListAdapter listAdapter;
    ArrayList<Orders> listDataHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        expListView = (ExpandableListView) findViewById(R.id.expandablelist);

        new ThreadOrders(MainActivity.this,new DownloadHandler()).execute();

        expListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Orders orders = listDataHeader.get(position);


            }
        });

    }


    private class DownloadHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            listDataHeader = (ArrayList<Orders>) msg.obj;

            listAdapter = new ExpandableListAdapter(MainActivity.this,listDataHeader);
            listAdapter.notifyDataSetChanged();
            expListView.setAdapter(listAdapter);


        }
    }

}
