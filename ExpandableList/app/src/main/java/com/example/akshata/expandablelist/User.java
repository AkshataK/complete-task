package com.example.akshata.expandablelist;

import java.io.Serializable;

/**
 * Created by akshata on 21/5/16.
 */
public class User implements Serializable{

    String name,profile_img_url;

    public User(String name, String profile_img_url) {
        this.name = name;
        this.profile_img_url = profile_img_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_img_url() {
        return profile_img_url;
    }

    public void setProfile_img_url(String profile_img_url) {
        this.profile_img_url = profile_img_url;
    }
}
