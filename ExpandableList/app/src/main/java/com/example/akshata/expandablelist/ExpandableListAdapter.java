package com.example.akshata.expandablelist;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by akshata on 19/5/16.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    ArrayList<Orders> listDataHeader;

    public ExpandableListAdapter(Context _context, ArrayList<Orders> listDataHeader) {
        this._context = _context;
        this.listDataHeader = listDataHeader;


    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {

        return listDataHeader.get(groupPosition).getArrItems().get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        return listDataHeader.get(groupPosition).getArrItems().size()+1;




    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        //* Note :- if we are returning more than one views then dont check convertview is == null;



        LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        final Orders orders = (Orders) getGroup(groupPosition);




      //  if (childPosition>=0 && childPosition<getChildrenCount(groupPosition)-1)
        if (childPosition<getChildrenCount(groupPosition)-1)
        {


                convertView = infalInflater.inflate(R.layout.childview, null);

           Items items = (Items) getChild(groupPosition,childPosition);

                TextView txtItemDescription, txtItemname, txtItemCount, txtItemPrice;

            txtItemname = (TextView) convertView.findViewById(R.id.item_name);


                txtItemDescription = (TextView) convertView.findViewById(R.id.item_description);

                txtItemCount = (TextView) convertView.findViewById(R.id.item_count);

                txtItemPrice = (TextView) convertView.findViewById(R.id.item_price);

                txtItemname.setText(items.getItem_name());
                txtItemDescription.setText(items.getDescription());
                txtItemCount.setText(String.valueOf(items.getItem_count()) + " items");
                txtItemPrice.setText("₹" + items.getItem_price());

            if (orders.getOrder_status().equals("ORDS0001"))
            {
                convertView.setBackgroundColor(Color.parseColor("#ffcdd2")); //red

            }
            else if (orders.getOrder_status().equals("ORDS0002"))
            {
                convertView.setBackgroundColor(Color.parseColor("#c8e6c9"));//green

            }
            else if (orders.getOrder_status().equals("ORDS0003"))
            {
                convertView.setBackgroundColor(Color.parseColor("#ef9a9a"));//dark red
            }
            else if (orders.getOrder_status().equals("ORDS0004"))
            {
                convertView.setBackgroundColor(Color.parseColor("#e8f5e9"));//light green
            }





        }else {
               // if (childPosition == getChildrenCount(groupPosition)-1)


            convertView = infalInflater.inflate(R.layout.totalview, null);

             final TextView txtcount, txtprice,txtResult;
             Button btnAccept,btnReject,btnPaid;


            txtcount = (TextView) convertView.findViewById(R.id.totalCount);
                txtprice = (TextView) convertView.findViewById(R.id.totalPrice);
            txtResult = (TextView) convertView.findViewById(R.id.txtResult);




            btnAccept = (Button) convertView.findViewById(R.id.btnAccept);
            btnReject = (Button) convertView.findViewById(R.id.btnReject);
            btnPaid = (Button) convertView.findViewById(R.id.btnPaid);

            btnPaid.setVisibility(View.INVISIBLE);

            txtcount.setText(String.valueOf(orders.total_items) + "items");
                txtprice.setText("₹" + String.valueOf(orders.total_price));

            btnAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    orders.setOrder_status("ORDS0002");
                    notifyDataSetChanged();

                }
            });

            btnReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    orders.setOrder_status("ORDS0003");

                    //txtResult.setText(orders.getOrder_id());

                    notifyDataSetChanged();
                }
            });

            btnPaid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    orders.setOrder_status("ORDS0004");

                    notifyDataSetChanged();


                }
            });


            if (orders.getOrder_status().equals("ORDS0001"))
            {
                convertView.setBackgroundColor(Color.parseColor("#ffcdd2")); //red

            }
            else if (orders.getOrder_status().equals("ORDS0002"))
            {
                convertView.setBackgroundColor(Color.parseColor("#c8e6c9"));//green
                btnAccept.setVisibility(View.INVISIBLE);
               // btnAccept.setText("Paid & Completed");
                btnReject.setVisibility(View.INVISIBLE);
                btnPaid.setText("Paid & Complete");
                btnPaid.setVisibility(View.VISIBLE);
            }
            else if (orders.getOrder_status().equals("ORDS0003"))
            {
                btnReject.setVisibility(View.INVISIBLE);
                convertView.setBackgroundColor(Color.parseColor("#ef9a9a"));//dark red
               // btnReject.setText("Order Rejected");
                btnAccept.setVisibility(View.INVISIBLE);
                txtResult.setText("Order Rejected");
            }
            else if (orders.getOrder_status().equals("ORDS0004"))
            {
                btnAccept.setVisibility(View.INVISIBLE);
                btnReject.setVisibility(View.INVISIBLE);
                convertView.setBackgroundColor(Color.parseColor("#e8f5e9"));//light green
                btnPaid.setVisibility(View.INVISIBLE);
                txtResult.setText("Order Completed");

            }


            }



        return convertView;

    }



    @Override
    public Object getGroup(int groupPosition) {
        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        Orders orders = (Orders) getGroup(groupPosition);

        User user = orders.user;


        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.groupview, null);
        }

        TextView txtuserName = (TextView) convertView
                .findViewById(R.id.txtusername);
        txtuserName.setTypeface(null, Typeface.BOLD);
           txtuserName.setText(user.getName());

        TextView txtOrderNo = (TextView) convertView.findViewById(R.id.OrderNo);
        txtOrderNo.setTypeface(null, Typeface.BOLD);
        txtOrderNo.setText(orders.getOrder_id());

        TextView ship = (TextView) convertView.findViewById(R.id.ship);
        ship.setTypeface(null, Typeface.BOLD);
        ship.setText(orders.getOrder_id());

        TextView pay = (TextView) convertView.findViewById(R.id.pay);
        pay.setTypeface(null, Typeface.BOLD);
        pay.setText(orders.getOrder_id());

        TextView txtdate = (TextView) convertView.findViewById(R.id.txtDate);
        txtdate.setTypeface(null, Typeface.BOLD);
        //DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        DateFormat dateFormat = new SimpleDateFormat("E,dd MMM yyyy HH:mm:ss");

        Date date = new Date();
        String thisdate = dateFormat.format(date);

        txtdate.setText(thisdate);

        if (orders.getShippingType().equals("ST0001"))
        {
            ship.setText("Pick, ");

        }
        else
        if (orders.getShippingType().equals("ST0002"))
        {
            ship.setText("Home Delivery, ");

        }

        if (orders.getPaymentType().equals("PAYT0001"))
        {
            pay.setText("Cash");

        }
        else
        if (orders.getPaymentType().equals("PAYT0002"))
        {
            pay.setText("Card");

        }

        ImageView profileImage = (ImageView) convertView.findViewById(R.id.profile);

        ImageView imageResult = (ImageView) convertView.findViewById(R.id.ImageResult);

        Picasso.with(_context).load("http://devapi.onegreendiary.com:1337/"+user.getProfile_img_url()).placeholder(R.drawable.profile).resize(100,100).into(profileImage);


        if (orders.getOrder_status().equals("ORDS0001"))
        {
            imageResult.setImageResource(android.R.drawable.ic_dialog_info);
            convertView.setBackgroundColor(Color.parseColor("#ffcdd2")); //red

        }
        else if (orders.getOrder_status().equals("ORDS0002"))
        {

            convertView.setBackgroundColor(Color.parseColor("#c8e6c9"));//green
        }
        else if (orders.getOrder_status().equals("ORDS0003"))
        {
            imageResult.setImageResource(android.R.drawable.ic_delete);
            convertView.setBackgroundColor(Color.parseColor("#ef9a9a"));//dark red
        }
        else if (orders.getOrder_status().equals("ORDS0004"))
        {
            imageResult.setImageResource(R.drawable.acceptimage);
            convertView.setBackgroundColor(Color.parseColor("#e8f5e9"));//light green
        }



        return convertView;

    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();

    }
}
